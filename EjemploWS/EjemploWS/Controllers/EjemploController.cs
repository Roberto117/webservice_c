﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EjemploWS.Controllers
{
    public class EjemploController : ApiController
    {
        [ActionName("Saludar")]
        [HttpGet]
        public String Saludar(String id) //Metodo - Tipo de retorno - Nombre del metodo - Variables que se reciben
        {
            String datos = "Hola " + id;

            return datos;
        }


        [ActionName("EjemploJson")]
        [HttpPost]
        public String EjemploJson(ObjetoEjemplo objeto) //Metodo - Tipo de retorno - Nombre del metodo - Variables que se reciben
        {
            return "Objeto Recibido";
        }
    }
    public class ObjetoEjemplo
    {
        public String Nombre { get; set; }

        public String Carrera { get; set; }
    }
}
