﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EjemploWS.Controllers
{
    public class LEDController : ApiController
    {
        [ActionName("led1stat")]
        [HttpGet]
        public String ON(String id) //Metodo - Tipo de retorno - Nombre del metodo - Variables que se reciben
        {
            String datos = "Hola " + id;

            return datos;
        }


        [ActionName("On")]
        [HttpGet]
        public String On(String id) //Metodo - Tipo de retorno - Nombre del metodo - Variables que se reciben
        {
            String datos = id;

            return datos;
        }


        [ActionName("Off")]
        [HttpGet]
        public String Off(String id) //Metodo - Tipo de retorno - Nombre del metodo - Variables que se reciben
        {
            String datos = "Hola " + id;

            return datos;
        }


        [ActionName("leds")]
        [HttpPost]
        public String leds(SolicitudLed objeto) //Metodo - Tipo de retorno - Nombre del metodo - Variables que se reciben
        {
            return "Objeto Recibido";
        }
    }
    public class SolicitudLed
    {
        public int State { get; set; }
    }


    public class RespuestaLed
    {
        public string Device { get; set; }
        public int State { get; set; }
    }
}
