﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTTPupt;

namespace EjemploWS.Vistas
{
    public partial class _default : System.Web.UI.Page
    {
        
        PeticionHTTP peticion = new PeticionHTTP("http://192.168.0.7");
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            peticion.PedirComunicacion("LED/On/led1off", MetodoHTTP.POST, TipoContenido.JSON);
            String respuesta = peticion.ObtenerJson();
        }

        /*
        protected void Button2_Click(object sender, EventArgs e)
        {
            peticion.PedirComunicacion("LED/Off/led1on", MetodoHTTP.GET, TipoContenido.JSON);
            String respuesta = peticion.ObtenerJson();
        }
        */

        protected void Button2_Click(object sender, EventArgs e)
        {
            SolicitudLed obj = new SolicitudLed
            {
                State = "led1on"
            };

            String enviar = JsonConvertidor.Objeto_Json(obj);

            peticion.PedirComunicacion("LED/leds", MetodoHTTP.POST, TipoContenido.JSON);
            peticion.enviarDatos(enviar);
            String respuesta = peticion.ObtenerJson();
        }

        /*
        protected void Button1_Click(object sender, EventArgs e)
        {
            peticion.PedirComunicacion("Ejemplo/Saludar/Roberto", MetodoHTTP.GET, TipoContenido.JSON);
            String respuesta = peticion.ObtenerJson();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            ObjetoEjemplo obj = new ObjetoEjemplo
            {
                Nombre = "Roberto",
                Carrera = "Software"
            };

            String enviar = JsonConvertidor.Objeto_Json(obj);

            peticion.PedirComunicacion("Ejemplo/EjemploJson", MetodoHTTP.POST, TipoContenido.JSON);
            peticion.enviarDatos(enviar);
            String respuesta = peticion.ObtenerJson();
        }
        */
    }
    /*
    public class ObjetoEjemplo
    {
        public String Nombre { get; set; }

        public String Carrera { get; set; }
    }
    */

    public class SolicitudLed
    {
        public String State { get; set; }
    }


    public class RespuestaLed
    {
        public string Device { get; set; }
        public int State { get; set; }
    }
}